// 1) Метод setTimeout() позволяет вызвать функцию один раз через определенный промежуток времени.
// Метод setInterval() позволяет вызывать функцию регулярно, повторяя вызов через определенный интервал времени.
// 2) При передече в функцию setTimeout() нулевой задержки функция сработает сразу же после выполнения выполнения текущего кода.
// 3) Для setInterval функция остаётся в памяти до тех пор, пока не будет вызван clearInterval. Функция ссылается на внешнее лексическое окружение, поэтому пока она существует, внешние переменные существуют тоже. Они могут занимать больше памяти, чем сама функция. Поэтому, если регулярный вызов функции больше не нужен, то лучше отменить его.


let imgList = document.querySelectorAll(".image-to-show");
let imgListArr = Array.from(imgList);
 console.log(imgListArr);
 const stopBtn = document.querySelector(".stopShow");
 const restart = document.querySelector(".restart");
 let showButton = setTimeout(function showBtn() {
   setTimeout(() => {
     stopBtn.classList.remove("hidden");
   }, 2000);
   setTimeout(() => {
     restart.classList.remove("hidden");
   }, 4000);
 });

let showStopped = false;
function showImgs(arr){
  for(const elem of arr){
  if (!showStopped){
    if(!elem.classList.contains("passive")){
      elem.classList.add("passive");
      if (arr.indexOf(elem) != arr.length - 1) {
        elem.nextElementSibling.classList.remove("passive");
        break;
      } else {
        arr[0].classList.remove("passive");
      }
    }    
    }
  }
} 

let sliderShow = setInterval(showImgs, 3000, imgListArr);

function stopShowing() {
  clearInterval(sliderShow);
  showStopped = false;
}
function restartShowing (){
  if (!showStopped) {
    sliderShow = setInterval(showImgs, 3000, imgListArr);
  }
};
stopBtn.addEventListener("click", stopShowing);
restart.addEventListener("click", restartShowing);